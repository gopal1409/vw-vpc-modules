resource "aws_eip" "bastion_eip" {
  depends_on = [module.ec2_public, module.vpc]
  instance   = module.ec2_public.id
  vpc        = true
  tags       = local.common_tags

  provisioner "local-exec" {
    
    command = "echo destroyed eip on 'date'  > destroy-time.txt"
    
    when    = destroy

  }
}

#eveything execute remotely
