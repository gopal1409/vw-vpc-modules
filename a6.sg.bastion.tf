module "bastion_server_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "public-bastion-sg-gopal"
  description = "sg for bstionhost"
  vpc_id      = module.vpc.vpc_id

  ingress_rules       = ["ssh-tcp", "http-80-tcp"]
  ingress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules        = ["all-all"]
  tags                = local.common_tags
}