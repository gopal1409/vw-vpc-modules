module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${local.name}-${var.vpc_name}"
  cidr = var.vpc_cidr_block

  azs             = var.vpc_availability_zones
  private_subnets = var.vpc_private_subnet
  public_subnets  = var.vpc_public_subnet

  database_subnets = var.vpc_database_subnet

  #enable_nat_gateway = true
  #enable_vpn_gateway = true

  tags = local.common_tags

}