resource "null_resource" "name" {
  depends_on = [ module.ec2_public ]
  ##to exeecute the null resource we are creating connection
  connection {
    type = "ssh"
    host = aws_eip.bastion_eip.public_ip
    user = "ec2-user"
    password = ""
    private_key = file("private-key/terraform-key.pem")
  }
  ##next we need to copy this private key inside your bastion or public instance
  provisioner "file" {
    source = "private-key/terraform-key.pem"
    destination = "/tmp/terraform-key.pem"
  }
  ###to execute
  provisioner "remote-exec" {
    inline = [ 
        "sudo chmod 400 /tmp/terraform-key.pem"
     ]
  }
  provisioner "local-exec" {
    command = "echo vpc created on 'date' and vpc ID: ${module.vpc.vpc_id} >> created-time.txt"
  }
}