###to define a varaible. 
variable "aws_region" {
  type    = string #number #map #listofstring
  default = "us-east-1"
}


variable "instance_keypair" {
  type    = string
  default = "terraform-key"
}
variable "business_devision" {
  type    = string
  default = "sap"
}

variable "environment" {
  type    = string
  default = "dev"
}
variable "instance_type" {
  default = "t3.nano"
}