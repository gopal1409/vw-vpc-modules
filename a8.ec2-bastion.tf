module "ec2_public" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name          = "${var.environment}-BastionHost-gopal"
  ami           = data.aws_ami.amzlinux2.id
  instance_type = var.instance_type
  key_name      = var.instance_keypair
  #monitoring             = true
  vpc_security_group_ids = [module.bastion_server_sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]

  tags = local.common_tags
}