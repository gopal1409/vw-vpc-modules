module "ec2_private" {
  source        = "terraform-aws-modules/ec2-instance/aws"
  depends_on    = [module.vpc]
  name          = "${var.environment}-vm-gopal"
  ami           = data.aws_ami.amzlinux2.id
  instance_type = var.instance_type
  key_name      = var.instance_keypair
  #monitoring             = true
  vpc_security_group_ids = [module.private_server_sg.security_group_id]
  subnet_id              = module.vpc.private_subnets[0]
  user_data              = file("${path.module}/app-script/app-script.sh")
  tags                   = local.common_tags
}